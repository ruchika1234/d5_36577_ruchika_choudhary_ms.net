﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book
{
    class Program
    {
        static void Main(string[] args)
        {
            book b = new book();
            b.AcceptDetails();
            b.PrintDetails();
            Console.ReadLine();
        }
    }

    struct book
    {
        private string title;
        private bool outofstock;
        private string author;
        private int isbn;
        private char index;
        private double price;

        void constructor()
        {
            this.title = "C#";
            this.outofstock = true;
            this.author = "Mahesh Sir";
            this.isbn = 1200;
            this.index = 'a';
            this.price = 5000;
        }

        void constructor(string title, bool outofstock, string author, int isbn, char index, double price)
        {
            this.title = title;
            this.outofstock = outofstock;
            this.author = author;
            this.isbn = isbn;
            this.index = index;
            this.price = price;
        }

        public void AcceptDetails()
        {
            Console.WriteLine("Enter Book Title:");
            this.title = Console.ReadLine();
            Console.WriteLine("Is book Out Of Stock :");
            this.outofstock = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("Enter Book author:");
            this.author = Console.ReadLine();
            Console.WriteLine("Enter International Standard Book Number:");
            this.isbn = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Book Index:");
            this.index = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Enter Book Price:");
            this.price = Convert.ToDouble(Console.ReadLine());
        }

        public void PrintDetails()
        {
            Console.WriteLine("Title :" + this.title);
            Console.WriteLine("Out of Stock :" + this.outofstock);
            Console.WriteLine("Author :" + this.author);
            Console.WriteLine("ISBN :" + this.isbn);
            Console.WriteLine("Index :" + this.index);
            Console.WriteLine("Price :" + this.price);
        }

        public string booktitle
        {
            get { return this.title; }
            set { this.title = value; }
        }

        public bool bookstock
        {
            get { return this.outofstock; }
            set { this.outofstock = value; }
        }


        public string bookauthor
        {
            get { return this.author; }
            set { this.author = value; }
        }


        public int bookisbn
        {
            get { return this.isbn; }
            set { this.isbn = value; }
        }


        public char bookindex
        {
            get { return this.index; }
            set { this.index = value; }
        }

        public double bookprice
        {
            get { return this.price; }
            set { this.price = value; }
        }

    }
}
