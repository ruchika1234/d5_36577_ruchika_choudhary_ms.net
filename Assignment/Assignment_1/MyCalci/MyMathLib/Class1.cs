﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMathLib
{
    public class Maths
    {
        public int Add(int x, int y)
        {
            return x + y;
        }

        public int Sub(int x, int y)
        {
            return x - y;
        }

        public int Mult(int x, int y)
        {
            return x * y;
        }

        public int Div(int x, int y)
        {
            return x / y;
        }

        public double FarenheitToCelcius(double F)
        {
            return (5 / 9) * (F - 32);
        }

        public double CelciusToFarenheit(double C)
        {
            return C * 1.8 + 32;
        }
    }
}
