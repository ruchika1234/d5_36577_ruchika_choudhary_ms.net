﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyMathLib;

namespace TempratureNS
{
    class TempratureConverter
    {
        static void Main(string[] args)
        {
            string iscontinue = "y";
            do
            {
                Console.WriteLine("Enter Your Choice");
                Console.WriteLine("1: Farenheit To Celcius 2: Celcius To Farenheit");
                double choice = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Value for Conversion");
                double x = Convert.ToInt32(Console.ReadLine());

                double result = 0;

                Maths temp = new Maths();

                switch (choice)
                {
                    case 1:
                        result = temp.FarenheitToCelcius(x);
                        break;
                    case 2:
                        result = temp.CelciusToFarenheit(x);
                        break;

                    default:
                        Console.WriteLine("Invalid Choice..Try Again!");
                        break;
                }

                Console.WriteLine(result);

                Console.WriteLine("Would you like to continue? y/n");
                iscontinue = Console.ReadLine();

            } while (iscontinue == "y");

            Console.ReadLine();
        }
    }
}
