﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyLoggerLib;
namespace Database
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Db  Needed");
            Console.WriteLine("1:Sql , 2: Oracle");

            int opchoice = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Operation Want to Do");
            Console.WriteLine("1:Insert , 2: Update 3: Delete");

            int choice = Convert.ToInt32(Console.ReadLine());

            ObjectFactory of = new ObjectFactory();
            Db db = of.GetMeSomeObject(opchoice);

            switch (choice)
            {
                case 1:
                    db.Insert();
                    break;
                case 2:
                    db.Update();
                    break;
                case 3:
                    db.Delete();
                    break;
                default:
                    Console.WriteLine("Invalid Input");
                    break;
            }
            Console.ReadLine();
        }
    }
    public class ObjectFactory
    {
        public Db GetMeSomeObject(int choice)
        {
            if (choice == 1)
            {
                return new Sql();
            }
            else
            {
                return new Oracle();
            }
        }
    }
    public abstract class Db
    {
        public abstract void Insert();
        public abstract void Update();
        public abstract void Delete();
    }
    public class Sql : Db
    {
        public override void Insert()
        {
            MyLogger.CurrentLogger.Log("Insert in Sql Db Done");
            Console.WriteLine("Insert in Sql Db Done");
        }
        public override void Update()
        {
            MyLogger.CurrentLogger.Log("Update in Sql Db Done");
            Console.WriteLine("Update in Sql Db Done");
        }
        public override void Delete()
        {
            MyLogger.CurrentLogger.Log("Delete in Sql Db Done");
            Console.WriteLine("Delete in Sql Db Done");
        }
    }

    public class Oracle : Db
    {
        public override void Insert()
        {
            MyLogger.CurrentLogger.Log("Insert in Oracle Db Done");
            Console.WriteLine("Insert in Oracle Db Done");
        }
        public override void Update()
        {
            MyLogger.CurrentLogger.Log("Insert in Oracle Db Done");
            Console.WriteLine("Update in Oracle Db Done");
        }

        public override void Delete()
        {
            MyLogger.CurrentLogger.Log("Insert in Oracle Db Done");
            Console.WriteLine("Delete in Oracle Db Done");
        }

    }
}
