﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MyLoggerLib
{
    public class MyLogger : ILogger
    {
        private static MyLogger _logger = new MyLogger();

        private MyLogger() { }

        public static MyLogger CurrentLogger
        {
            get { return _logger; }

        }

        public void Log(string message)
        {
            string filename = "Log" + DateTime.Now.ToString("ddMMyyyy") + ".csv";
            FileStream fs = new FileStream(@"G:\MS.Net\Assignment\Assignmetn 5\Assignment-5\"+filename, FileMode.Append, FileAccess.Write);

            StreamWriter writer = new StreamWriter(fs);

            writer.WriteLine(DateTime.Now.ToString() + "," + message);

            Console.WriteLine(DateTime.Now.ToString() + "," + message);
            fs.Flush();
            writer.Close();
            fs.Close();

        }
    }
}
