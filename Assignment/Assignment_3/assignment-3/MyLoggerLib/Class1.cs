﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mylog
{

    public class Logger
    {
        private static Logger _logger = new Logger();

        private Logger()
        {
            Console.WriteLine("create object successfully");
        }

        public static Logger CurrentLoger
        {
            get { return _logger; }
        }

        public void log(string msg)
        {
            Console.WriteLine("logged at : " + msg + "@" + DateTime.Now.ToString());
        }

    }
}
