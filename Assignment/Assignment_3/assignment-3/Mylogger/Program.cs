﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class Program
    {
        static void Main(string[] args)
        {
            MyLoggerLib.CurrentLogger.log("this is log msg");
            Console.ReadLine();

        }
    }

    class MyLoggerLib
    {

        private static MyLoggerLib myLoggerLib = new MyLoggerLib();
        private MyLoggerLib()
        {
            Console.WriteLine("create object of MyLoggerLib \n");

        }

        /*public int get() { }*/

        public static MyLoggerLib CurrentLogger
        {
            get { return myLoggerLib; }
        }

        public void log(string msg)
        {
            Console.WriteLine("logger Msg" + msg + "@" + DateTime.Now.ToString());
        }


    }
}
