﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Person
    {
        private string _name;

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        private bool _gender;

        public bool gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        private Date _birth;

        public Date birth
        {
            get { return _birth; }
            set { _birth = value; }
        }

        private string _address;

        public string address
        {
            get { return _address; }
            set { _address = value; }
        }

        private string _EmailID;
        private string gender1;

        public string EmailID
        {
            get { return _EmailID; }
            set { _EmailID = value; }
        }


        public Person()
        {
            this._name = "";
            this._gender = true;
            this._address = "";
            this._EmailID = "";
        }

        public Person(string name, bool gender, string address, Date birth, string email)
        {
            this.name = name;
            this.gender = gender;
            this.address = address;
            this.birth = birth;
            this.EmailID = email;
        }

        public Person(string name, string gender1, Date birth, string address, string email)
        {
            this.name = name;
            this.gender1 = gender1;
            this.birth = birth;
            this.address = address;
        }

        public string Gender()
        {
            return this.gender == true ? "female" : "male";
        }

        public override string ToString()
        {
            return " Name : " + this.name + ", Gender : " + Gender() + ", Date : " + this.birth.ToString()  + ", Address : " + this.address + ", Email-id :" + this.EmailID;
        }

        public virtual void AcceptDetails()
        {
            Console.WriteLine("enter name");
            this.name = Console.ReadLine();

            Console.WriteLine("Gender: select option male or female");
            if(Console.ReadLine() == "female")
            {
                this.gender = true;
            }
            else
            {
                this.gender = false;
            }

            Console.WriteLine("enter Address");
            this.address = Console.ReadLine();

            Console.WriteLine("Please Enter Birth in format dd/mm/yyyy");
            Date d = new Date();
            this.birth = d;
            this.birth.AcceptDetails();

            Console.WriteLine("enter Email id");
            this.EmailID = Console.ReadLine();


        }

        public virtual void PrintDetails()
        {
            Date d = new Date();

            d.day = Convert.ToInt32(DateTime.Now.ToString("dd"));
            d.month = Convert.ToInt32(DateTime.Now.ToString("MM"));
            d.year = Convert.ToInt32(DateTime.Now.ToString("yyyy"));
            Console.WriteLine("Name is :" + this.name);
            Console.WriteLine("Gender is :" + this.gender);
            Console.WriteLine("Date of Birth :");
            this.birth.PrintDetails();
            Console.WriteLine("Address is :" + this.address);
            Console.WriteLine("Email is :" + this.EmailID);
            Console.WriteLine("Age is :" + Date.differenceOfYear(this.birth, d));
        }


        /* Date tdate;
         public int getAge()
         {

             string td = DateTime.Now.ToString();
             DateTime datevalue = (Convert.ToDateTime(td.ToString()));

             tdate.day = Convert.ToInt32(datevalue.Day.ToString());
             tdate.month = Convert.ToInt32(datevalue.Month.ToString());
             tdate.year = Convert.ToInt32(datevalue.Year.ToString());

             return Date.differenceOfYear(this._birth, tdate);

         }*/

    }
}
