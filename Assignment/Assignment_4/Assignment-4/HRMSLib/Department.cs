﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Department
    {
        private int _DeptNo;
        private string _DeptName;
        private string _location;

        public string location
        {
            get { return _location; }
            set { _location = value; }
        }


        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }


        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }

        public Department()
        {
            this._DeptNo = 0;
            this._DeptName = "";
            this._location = "";
        }

        public Department(int deptno, string deptName, string location)
        {
            this._DeptNo = deptno;
            this._DeptName = deptName;
            this._location = location;
        }

        public override string ToString()
        {
            return "Dept no." + this.DeptNo.ToString() + "Dept name" + this.DeptName + "location " + this.location;
        }

        public void AcceptDetails()
        {
            Console.WriteLine("Enter dept no.");
            this.DeptNo = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("enter dept name");
            this.DeptName = Console.ReadLine();

            Console.WriteLine("Enter location");
            this.location = Console.ReadLine();
        }

        public void PrintDetails()
        {
            Console.WriteLine("Dept no: " + this.DeptNo);
            Console.WriteLine("Dept name: " + this.DeptName);
            Console.WriteLine("location: " + this.location);
        }

    }
}
