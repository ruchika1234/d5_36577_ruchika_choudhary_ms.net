﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Employee: Person
    {
        private static int _ID = 0;
        private double _salary;
        private double _hra;
        private double _da;
        private string _designation;
        private EmployeeTypes _empType;
        private string _passcode;
        private int _DeptNo;
        private Date _HireDate;

        public Date HireDate
        {
            get { return _HireDate; }
            set { _HireDate = value; }
        }


        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }


        public string passcode
        {
            get { return _passcode; }
            set { _passcode = value; }
        }



        public int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }


        public double salary
        {
            get { return _salary; }
            set { _salary = value; }
        }


        public double hra
        {
            get { return _hra; }
            set { _hra = value; }
        }


        public double da
        {
            get { return _da; }
            set { _da = value; }
        }


        public string designation
        {
            get { return _designation; }
            set { _designation = value; }
        }

        public Employee()
        {
            Employee._ID++;
            this.ID = Employee._ID;
            this.salary = 0;
            this.hra = 0;
            this.da = 0;
            this.designation = "";
            this._empType = 0;
            this.passcode = "";
            this.DeptNo = 0;
        }

        public Employee(string name, string gender, Date birth, string address, string email ,double salary, double hra, double da, string designation, string passcode, int deptNo, Date hire_date) : base(name, gender, birth, address, email)
        {
            Employee._ID++;
            this.ID = Employee._ID;
            this.salary = salary;
            this.hra = hra;
            this.da = da;
            this.designation = designation;
            this._empType = 0;
            this.passcode = passcode;
            this.HireDate = hire_date;
            this.DeptNo = deptNo;
        }

        public override void AcceptDetails()
        {
            base.AcceptDetails();

            Console.WriteLine("Please Enter Salary");
            this.salary = Convert.ToDouble(Console.ReadLine());

            this.hra = (this.salary * 4) / 10;

            this.da = (this.salary / 10);

            Console.WriteLine("Please Enter Designation");
            this.designation = Console.ReadLine();

            Console.WriteLine("Please Enter Trainee, Temporary or Permanent");
            string et = Console.ReadLine();

            if (et == "Trainee")
            {
                this._empType = EmployeeTypes.Trainee;
            }
            else if (et == "Temporary")
            {
                this._empType = EmployeeTypes.Temporary;
            }
            else if (et == "Permanent")
            {
                this._empType = EmployeeTypes.Permanent;
            }
            else
            {
                this._empType = 0;
            }


            Console.WriteLine("Please Enter Passcode");
            this.passcode = Console.ReadLine();


            Console.WriteLine("Please Enter Deptno");
            this.DeptNo = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("Please Enter hiring date");
            Date d = new Date();
            this.HireDate = d;
            this.HireDate.AcceptDetails();



        }

        public override void PrintDetails()
        {

            base.PrintDetails();
            Console.WriteLine("Employee id :" + this.ID);
            Console.WriteLine("Net Salary :" + this.NetSalary());
            Console.WriteLine("Designation :" + this.designation);
            Console.WriteLine("Employee Type :" + this._empType);
            Console.WriteLine("Passcode :" + this.passcode);
            Console.WriteLine("Dept no :" + this.DeptNo);
            Console.WriteLine("Hiring Date: " + this.HireDate.ToString());

           // this.HireDate.PrintDetails();


        }


        public virtual double NetSalary()
        {
            return this.salary + this.hra + this.da;
        }

        public override string ToString()
        {
            return base.ToString() + "salary " + this.salary + "HRA " + this.hra + "DA" + this.da + "Designation " + this.designation + "Emp type " + this._empType.ToString() + "passcode " + this.passcode + "dept no. " + this.DeptNo + "hiring date" + this.HireDate.ToString();
        }

    }
}
