﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class Date   
    {
        private int _day;

        public int day
        {
            get { return _day; }
            set
            {
                if (value >= 1 && value <= 31)
                {
                    _day = value;
                }
                else
                {
                    Console.WriteLine("Please enter Date between 1-31");

                }
            }
        }
        private int _month;

        public int month
        {
            get { return _month; }
            set
            {
                if (value >= 1 && value <= 12)
                {
                    _month = value;
                }
                else
                {
                    Console.WriteLine("Please enter month between 1-12");
                }
            }
        }

        private int _year;
        public int year
        {
            get { return _year; }
            set
            {
                if (value >= 1901 && value <= 2100)
                {
                    _year = value;
                }
                else
                {
                    Console.WriteLine("Please enter year between 1901-2100");
                }
            }
        }

        public Date()
        {
            this._day = 7;
            this._month = 10;
            this._year = 2020;
        }

        public Date(int day, int month, int year)
        {
            this._day = day;
            this._month = month;
            this._year = year;
        }

        /* public virtual string getDetails()
         {
             return this.day + "/" + this.month + "/" + this.year;
         }*/

        public override string ToString()
        {
            return this.day + "/" + this.month + "/" + this.year;
        }

        public virtual void AcceptDetails()
        {
            Console.WriteLine("Enter day");
            this.day = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Month");
            this.month = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Year");
            this.year = Convert.ToInt32(Console.ReadLine());

           // this.getDetails();
        }

        public virtual void PrintDetails()
        {
            Console.WriteLine(this.day + "/" + this.month + "/" + this.year);
        }

        public static int differenceOfYear(Date first, Date second)
        {
            return Math.Abs(first.year - second.year);
        }

    }
}

