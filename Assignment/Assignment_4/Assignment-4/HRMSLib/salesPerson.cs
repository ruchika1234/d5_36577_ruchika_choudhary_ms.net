﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class salesPerson: Employee
    {
        private double _commision;

        public double commision
        {
            get { return _commision; }
            set { _commision = value; }
        }

        public salesPerson()
        {
            this.commision = 0;
        }

        public salesPerson(double comm)
        {
            this.commision = comm;
        }

        public override string ToString()
        {
            return base.ToString() + "commision " + this.commision;
        }

        public override void AcceptDetails()
        {
            base.AcceptDetails();
            Console.WriteLine("Please enter commision");
            this.commision = Convert.ToDouble(Console.ReadLine());
        }

        public override void PrintDetails()
        {
            base.PrintDetails();
            Console.WriteLine("Commision :" + this.commision);
        }

        public override double NetSalary()
        {
            return base.NetSalary() + this.commision;
        }
    }
}
