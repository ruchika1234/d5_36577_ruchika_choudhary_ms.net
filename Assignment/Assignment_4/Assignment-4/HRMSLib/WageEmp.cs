﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRMSLib
{
    public class WageEmp: Employee
    {
        private int _hour;
        private int _rate;

        public int rate
        {
            get { return _rate; }
            set { _rate = value; }
        }


        public int hour
        {
            get { return _hour; }
            set { _hour = value; }
        }

        public WageEmp()
        {
            this._hour = 0;
            this._rate = 0;
        }

        public WageEmp(int hor, int rate)
        {
            this._hour = hour;
            this._rate = rate;
        }

        public override string ToString()
        {
            return base.ToString() + "Hours " + this.hour.ToString() + "rate " + this.rate.ToString();
        }

        public override void AcceptDetails()
        {
            base.AcceptDetails();
            Console.WriteLine("Please enter Hour");
            this.hour = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please enter Rate");
            this.rate = Convert.ToInt32(Console.ReadLine());
        }

        public override void PrintDetails()
        {
            base.PrintDetails();
            Console.WriteLine("Hour :" + this.hour);
            Console.WriteLine("Rate :" + this.rate);
        }

        public override double NetSalary()
        {
            return this.rate * this.hour;
        }
    }
}
