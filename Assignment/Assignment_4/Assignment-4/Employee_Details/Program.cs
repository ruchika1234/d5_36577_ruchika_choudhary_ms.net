﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRMSLib;

namespace Employee_Details
{
    class Program
    {
        static void Main(string[] args)
        {
            string isContinue = "yes";
            do
            {
                Console.WriteLine("Enter Employee Types");
                Console.WriteLine("1: Employee , 2: SalesPerson Employee, 3: WageEmployees, 4: Department ");

                int choice = Convert.ToInt32(Console.ReadLine());
                Person p;

                switch (choice)
                {
                    case 1:
                        p = new Employee();
                        p.AcceptDetails();
                        p.PrintDetails();
                        break;

                    case 2:
                        p = new salesPerson();
                        p.AcceptDetails();
                        p.PrintDetails();
                        break;

                    case 3:
                        p = new WageEmp();
                        p.AcceptDetails();
                        p.PrintDetails();
                        break;

                    case 4:
                        Department d = new Department();
                        d.AcceptDetails();
                        d.PrintDetails();
                        break;

                    default:
                        Console.WriteLine("Invalid Choice");
                        break;
                }
                Console.WriteLine("Would you like to continue yes/no");
                isContinue = Console.ReadLine();

                if (isContinue == "no")
                {
                    break;
                }

            } while (isContinue == "yes");

            /* Date d = new Date();
             d.accept_Details();
             Console.WriteLine(d.getDetails());

             Person p = new Person();
             p.AcceptDetails();
             Console.WriteLine(p.getDetails());

             Console.ReadLine();*/

        }
    }
}
