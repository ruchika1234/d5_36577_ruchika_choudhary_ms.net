﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR_Lib
{
    public class Employee
    {
        private int _EmpNO;
        private string _Name;
        private string _Designation;
        private double _Salary;
        private double _Commision;
        private int _DeptNo;

        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }


        public double Commision
        {
            get { return _Commision; }
            set { _Commision = value; }
        }


        public double Salary
        {
            get { return _Salary; }
            set { _Salary = value; }
        }


        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }


        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int EmpNO
        {
            get { return _EmpNO; }
            set { _EmpNO = value; }
        }

        public override string ToString()
        {
            return this.EmpNO + "  " + this.Name + "  " + this.Designation + "  " + this.Salary + "  " + this.Commision + "  " + this.DeptNo;
        }

    }
}
