﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR_Lib
{
    public class Department
    {
        private int _DeptNo;
        private string _DeptName;
        private string _Location;

        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }


        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }


        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }

        public override string ToString()
        {
            return this.DeptNo + "  " + this.DeptName + "  " + this.Location;
        }

       
    }
}
