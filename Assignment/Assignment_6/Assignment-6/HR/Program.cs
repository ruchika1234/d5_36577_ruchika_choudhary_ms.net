﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR_Lib;
using System.IO;

namespace HR
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Department details from dept.csv

            FileStream fs = new FileStream(@"G:\MS.Net Data\msnet\assignments\dept.csv", FileMode.Open, FileAccess.Read);

            StreamReader reader = new StreamReader(fs);

            Dictionary<int, Department> DeptList = new Dictionary<int, Department>();

            string deptstring;
            while ((deptstring = reader.ReadLine()) != null)
            {
                //  deptstring = "50,Training,Karad";
                string[] deptDetails = deptstring.Split(',');
                Department dept = new Department();
                dept.DeptNo = int.Parse(deptDetails[0]);
                dept.DeptName = deptDetails[1];
                dept.Location = deptDetails[2];
                DeptList.Add(dept.DeptNo, dept);
            }

            Console.WriteLine("-----------Department info-------");

            foreach (KeyValuePair<int, Department> d in DeptList)
            {
                Console.WriteLine(d.ToString());
            }

            #endregion

            #region Employee Details From emp.csv

            FileStream fs1 = new FileStream(@"G:\MS.Net Data\msnet\assignments\emp.csv", FileMode.Open, FileAccess.Read);

            StreamReader reader1 = new StreamReader(fs1);

            Dictionary<int, Employee> EmpList = new Dictionary<int, Employee>();

            string empstring;
            while ((empstring = reader1.ReadLine()) != null)
            {
                string[] empDetails = empstring.Split(',');
                Employee emp = new Employee();
                emp.EmpNO = int.Parse(empDetails[0]);
                emp.Name = empDetails[1];
                emp.Designation = empDetails[2];
                emp.Salary = double.Parse(empDetails[3]);
                emp.Commision = double.Parse(empDetails[4]);
                emp.DeptNo = int.Parse(empDetails[5]);
                EmpList.Add(emp.EmpNO, emp);
            }

            Console.WriteLine("-----------Employee info-------");

            foreach (KeyValuePair<int, Employee> e in EmpList)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadLine();

            fs.Flush();
            fs1.Flush();
            fs.Close();
            fs1.Close();


            #endregion

            #region  LocationWithSingleDept(DeptList)

            List<string> LocationWithSingleDept(Dictionary<int, Department> deptList)
            {
                List<string> loc = new List<string>();
                foreach (KeyValuePair<int, Department> d in deptList)
                {
                    if (loc.Contains(d.Value.Location))
                    {
                        loc.Remove(d.Value.Location);
                    }
                    else
                    {
                        loc.Add(d.Value.Location);
                    }
                }

                return loc;
            }

            Console.WriteLine(" Location With Single Department");

            /*  List<string> location = new List<string>();
              location = LocationWithSingleDept(DeptList);
              foreach (var l in location)
              {
                  Console.WriteLine(l);
              }*/

            var result = (from dept in DeptList.Values group dept by dept.Location into d where d.Count() == 1 select new { location = d.Key }).ToList();
            foreach (var item in result)
            {
                Console.WriteLine(item.location);
            }

            #endregion

            #region FindDeptsWithNoEmps

            /* List<string> FindDeptsWithNoEmps(Dictionary<int, Employee> empList)
             {
                 int test = 0;
                 List<string> DeptName = new List<string>();
                 foreach (KeyValuePair<int, Department> dept in DeptList)
                 {
                     test = 0;
                     foreach (KeyValuePair<int, Employee> emp in empList)
                     {
                         if (dept.Value.DeptNo == emp.Value.DeptNo)
                         {
                             test++;
                         }
                         else
                         {
                             continue;
                         }
                     }

                     if (test == 0)
                     {
                         DeptName.Add(dept.Value.DeptName);
                     }
                 }

                 return DeptName;
             }


             Console.WriteLine("----------Department With No Employees---------");

             List<string> st = new List<string>(); 
             st = FindDeptsWithNoEmps(EmpList);
             foreach (var name in st)
             {
                 Console.WriteLine(name);
             }*/

            var res = (from dept in DeptList.Values
                       join emp in EmpList.Values on dept.DeptNo equals emp.DeptNo into data
                       where data.Count() == 0 select dept).ToList();

            foreach (Department dept in res)
            {
                Console.WriteLine(dept.DeptName);
            }

            #endregion

            #region  Calcluate_Total_Salary(EmpList)

            /* double Calcluate_Total_Salary(Dictionary<int, Employee> empList)
             {
                 double total_salary = 0;
                 foreach (KeyValuePair<int, Employee> emp in empList)
                 {
                     total_salary += emp.Value.Salary + emp.Value.Commision;
                 }

                 return total_salary;
             }

             Console.WriteLine("--------Total salary of Employees-------");

             double salary = Calcluate_Total_Salary(EmpList);
             Console.WriteLine("Total Salary of all Employees " + salary);
 */
            var res1 = (from emp in EmpList.Values select emp.Salary + emp.Commision).Sum();
            Console.WriteLine("TotalSal: " + res1);

            #endregion

            #region List<Employee> GetAllEmployeesByDept(int DeptNo)

            /*  List<Employee> GetAllEmployeesByDept(int DeptNo)
              {
                  List<Employee> emp = new List<Employee>();
                  foreach (KeyValuePair<int, Employee> e in EmpList)
                  {
                      if(e.Value.DeptNo == DeptNo)
                      {
                          emp.Add(e.Value);
                      }
                  }

                  return emp;
              }

              Console.WriteLine("-------All Employees by Department---------");

              List<Employee> employee = new List<Employee>();
              employee = GetAllEmployeesByDept(30);
              foreach (var detail in employee)
              {
                  Console.WriteLine(detail.ToString());
              }
  */

            Console.WriteLine("Enter DeptNo: ");
            int deptNo = Convert.ToInt32(Console.ReadLine());
            var res2 = (from emp in EmpList.Values where emp.DeptNo == deptNo select emp).ToList();
            foreach (Employee emp in res2)
            {
                Console.WriteLine(emp);
            }


            #endregion

            #region DeptwiseStaffCount(EmpList)

            /*Dictionary<int, int> DeptwiseStaffCount(Dictionary<int, Employee> empList)
            {
                int count = 0;
                Dictionary<int, int> countOfEmployee = new Dictionary<int, int>();
                foreach (KeyValuePair<int, Department> dept in DeptList)
                {
                    count = 0;
                    foreach (KeyValuePair<int, Employee> emp in empList)
                    {
                        if (dept.Value.DeptNo == emp.Value.DeptNo)
                        {
                            count++;
                        }
                    }

                    countOfEmployee.Add(dept.Value.DeptNo, count);
                }

                return countOfEmployee;
            }

            Console.WriteLine("-------Department wise staff Count--------");

            Dictionary<int, int> staffEmp = new Dictionary<int, int>();
            staffEmp = DeptwiseStaffCount(EmpList);
            foreach (KeyValuePair<int, int> e in staffEmp)
            {
                Console.WriteLine(e);
            }*/

            var res3 = (from emp in EmpList.Values group emp by emp.DeptNo into g select g).ToDictionary(g => g.Key, g => g.Count());
            foreach (int key in res3.Keys)
            {
                Console.WriteLine(key + " " + res3[key]);
            }

            #endregion

            #region DeptwiseAvgSal(EmpList)
            /*
                        Dictionary<int, Double> DeptwiseAvgSal(Dictionary<int, Employee> empList)
                        {
                            double Average_Salary = 0;
                            Dictionary<int, double> AvgSalary = new Dictionary<int, double>();
                            foreach (KeyValuePair<int, Department> dept in DeptList)
                            {
                                Average_Salary = 0;
                                foreach (KeyValuePair<int, Employee> emp in empList)
                                {
                                    if (dept.Value.DeptNo == emp.Value.DeptNo)
                                    {
                                        Average_Salary += emp.Value.Salary + emp.Value.Commision;
                                    }
                                }

                                AvgSalary.Add(dept.Value.DeptNo, Average_Salary);
                            }

                            return AvgSalary;
                        }

                        Console.WriteLine("---------Department Wise Average Salary---------");

                        Dictionary<int, double> sal = new Dictionary<int, double>();
                        sal = DeptwiseAvgSal(EmpList);
                        foreach (KeyValuePair<int, double> average in sal)
                        {
                            Console.WriteLine(average);
                        }*/

            var res4 = (from emp in EmpList.Values group emp by emp.DeptNo into g select g).ToDictionary(g => g.Key, g => g.Average(emp => emp.Salary));
            foreach (KeyValuePair<int, double> item in res4)
            {
                Console.WriteLine("DeptNo: " + item.Key + " " + "AVG Salary " + item.Value);
            }

            #endregion

            #region DeptwiseMinSal(EmpList)


            /*  Dictionary<int, Double> DeptwiseMinSal(Dictionary<int, Employee> empList)
              {
                  double minimum_Salary = double.MaxValue;
                  Dictionary<int, double> MINSalary = new Dictionary<int, double>();
                  List<double> list = new List<double>();
                  foreach (KeyValuePair<int, Department> dept in DeptList)
                  {
                      list.Clear();
                      foreach (KeyValuePair<int, Employee> emp in empList)
                      {
                          if (dept.Value.DeptNo == emp.Value.DeptNo)
                          {
                              list.Add(emp.Value.Salary);
                          }

                      }

                      minimum_Salary = double.MaxValue;
                      //Console.WriteLine("list");
                      bool isEmpty = !list.Any();
                      if (isEmpty)
                      {
                          //Console.WriteLine("Empty");
                          minimum_Salary = 0.0;
                      }
                      else
                      {
                          //Console.WriteLine("List is not empty");
                      }
                      foreach (double d in list)
                      {
                          //Console.WriteLine(d);
                          if (minimum_Salary > d)
                          {
                              minimum_Salary = d;
                          }


                      }

                      MINSalary.Add(dept.Value.DeptNo, minimum_Salary);
                  }

                  return MINSalary;
              }

              Console.WriteLine("------Department Wise MIN Salary--------");

              Dictionary<int, double> min_sal = new Dictionary<int, double>();
              min_sal = DeptwiseMinSal(EmpList);
              foreach (KeyValuePair<int, double> s in min_sal)
              {
                  Console.WriteLine(s);
              }
  */

            var res5 = (from emp in EmpList.Values group emp by emp.DeptNo into g select g).ToDictionary(g => g.Key, g => g.Min(emp => emp.Salary));
            foreach (KeyValuePair<int, double> item in res5)
            {
                Console.WriteLine("DeptNo: " + item.Key + " " + "MIN Salary " + item.Value);
            }

            #endregion

            #region employee names along with department names


            var res6 = (from emp in EmpList
                        join dept in DeptList
                         on emp.Value.DeptNo equals dept.Value.DeptNo
                        where emp.Value.DeptNo == dept.Value.DeptNo
                        select new
                        {
                            Eno = emp.Key,
                            Ename = emp.Value,
                            Deptname = dept.Value.DeptName
                        }).ToDictionary(x => x.Eno, x => String.Concat(x.Ename, ",", x.Deptname));

            foreach (KeyValuePair<int, string> index in res6)
            {
                string[] name = index.Value.Split(',');
                Console.WriteLine("Emp Number = " + index.Key + ", Emp Name = " + name[0] + ", DeptName = " + name[1]);
            }
            #endregion

            Console.ReadLine();

        }


    }

    
}
