﻿using System.Text;
using System.Threading.Tasks;

namespace MathsLib
{
    public class Maths
    {
        public int Add(int x, int y)
        {
            return x + y;
        }
        public int Sub(int x, int y)
        {
            return x - y;
        }
    }
}
