﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Collections;
using System.Xml.Serialization;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Binary Serialization

            /* Emp emp = new Emp();

             Console.WriteLine("Enter No.");
             emp.No = Convert.ToInt32(Console.ReadLine());

             Console.WriteLine("Enter Name");
             emp.Name = Console.ReadLine();

             FileStream fs = new FileStream(@"G:\MS.Net\Classwork\Day8\Day8_Demos\Data.txt", FileMode.OpenOrCreate, FileAccess.Write);

             BinaryFormatter specialWriter = new BinaryFormatter();

             specialWriter.Serialize(fs, emp);

             specialWriter = null;
             fs.Flush();
             fs.Close();

             Console.ReadLine();
 */
            #endregion

            #region Binary De-Serialization
            /*
                        FileStream fs = new FileStream(@"G:\MS.Net\Classwork\Day8\Day8_Demos\Data.txt", FileMode.Open, FileAccess.Read);

                        BinaryFormatter specialReader = new BinaryFormatter();

                        object obj = specialReader.Deserialize(fs);
                        if(obj is Emp)
                        {
                            Emp e = (Emp)obj;
                            Console.WriteLine(e.getDetails());
                        }
                        else
                        {
                            Console.WriteLine("unknown data");
                        }

                        specialReader = null;
                        fs.Flush();
                        fs.Close();

                        Console.ReadLine();*/

            #endregion

            #region Binary Serialization of Arraylist

            /*  Emp emp = new Emp();

              Console.WriteLine("Enter No.");
              emp.No = Convert.ToInt32(Console.ReadLine());

              Console.WriteLine("Enter Name");
              emp.Name = Console.ReadLine();

              Book book = new Book();

              Console.WriteLine("Enter ISBN: ");
              book.ISBN = Convert.ToInt32(Console.ReadLine());

              Console.WriteLine("Enter Title: ");
              book.title = Console.ReadLine();

              ArrayList arr = new ArrayList();
              arr.Add(emp);
              arr.Add(book);

              FileStream fs = new FileStream(@"G:\MS.Net\Classwork\Day8\Day8_Demos\Data.txt", FileMode.OpenOrCreate, FileAccess.Write);

              BinaryFormatter specialWriter = new BinaryFormatter();

              specialWriter.Serialize(fs, arr);

              specialWriter = null;
              fs.Flush();
              fs.Close();

              Console.ReadLine();*/

            #endregion

            #region Binary De-Serialization of ArrayList

            /*  FileStream fs = new FileStream(@"G:\MS.Net\Classwork\Day8\Day8_Demos\Data.txt", FileMode.Open, FileAccess.Read);

              BinaryFormatter specialReader = new BinaryFormatter();

              object obj = specialReader.Deserialize(fs);
              if (obj is ArrayList)
              {
                  ArrayList arr = new ArrayList();
                  foreach (object o in arr)
                  {
                      if(o is Emp)
                      {
                          Emp e = (Emp)o;
                          Console.WriteLine(e.getDetails());
                      }
                      else if(o is Book)
                      {
                          Book b = (Book)o;
                          Console.WriteLine(b.getDetails());
                      }
                      else
                      {
                          Console.WriteLine("Unknown data in arr");
                      }

                  }
              }
              else
              {
                  Console.WriteLine("unknown data");
              }

              specialReader = null;
              fs.Flush();
              fs.Close();

              Console.ReadLine();*/
            #endregion

            #region XML Serialization

            /* Emp emp = new Emp();

             Console.WriteLine("Enter No.");
             emp.No = Convert.ToInt32(Console.ReadLine());

             Console.WriteLine("Enter Name");
             emp.Name = Console.ReadLine();

             FileStream fs = new FileStream(@"G:\MS.Net\Classwork\Day8\Day8_Demos\Data.xml", FileMode.OpenOrCreate, FileAccess.Write);

             XmlSerializer specialWriter = new XmlSerializer(typeof(Emp));

             specialWriter.Serialize(fs, emp);

             specialWriter = null;
             fs.Flush();
             fs.Close();

             Console.ReadLine();*/
            #endregion

            #region XML De-Serialization
            /*
                        FileStream fs = new FileStream(@"G:\MS.Net\Classwork\Day8\Day8_Demos\Data.xml", FileMode.Open, FileAccess.Read);

                        XmlSerializer specialReader = new XmlSerializer(typeof(Emp));

                        object obj = specialReader.Deserialize(fs);
                        if(obj is Emp)
                        {
                            Emp e = (Emp)obj;
                            Console.WriteLine(e.getDetails());
                        }
                        else
                        {
                            Console.WriteLine("unknown data");
                        }

                        specialReader = null;
                        fs.Flush();
                        fs.Close();

                        Console.ReadLine();
*/
            #endregion

            #region SOAP Serialization

            /*Emp emp = new Emp();

            Console.WriteLine("Enter No.");
            emp.No = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Name");
            emp.Name = Console.ReadLine();

            FileStream fs = new FileStream(@"G:\MS.Net\Classwork\Day8\Day8_Demos\DataSOAP.xml", FileMode.OpenOrCreate, FileAccess.Write);

            SoapFormatter specialWriter = new SoapFormatter();

            specialWriter.Serialize(fs, emp);

            specialWriter = null;
            fs.Flush();
            fs.Close();

            Console.ReadLine();*/
            #endregion

            #region XML De-Serialization

            FileStream fs = new FileStream(@"G:\MS.Net\Classwork\Day8\Day8_Demos\DataSOAP.xml", FileMode.Open, FileAccess.Read);

                        SoapFormatter specialReader = new SoapFormatter();

                        object obj = specialReader.Deserialize(fs);
                        if(obj is Emp)
                        {
                            Emp e = (Emp)obj;
                            Console.WriteLine(e.getDetails());
                        }
                        else
                        {
                            Console.WriteLine("unknown data");
                        }

                        specialReader = null;
                        fs.Flush();
                        fs.Close();

                        Console.ReadLine();
            #endregion
        }
    }


    [Serializable]
    public class Book
    {
        private int _ISBN;
        private string _title;

        public string title
        {
            get { return _title; }
            set { _title = value; }
        }


        public int ISBN
        {
            get { return _ISBN; }
            set { _ISBN = value; }
        }

        public string getDetails()
        {
            return ISBN.ToString() + " - " + title;
        }

    }

    [Serializable]
    public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return No.ToString() + " - " + Name;
        }

    }
}
