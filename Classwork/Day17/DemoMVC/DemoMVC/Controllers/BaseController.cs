﻿using DemoMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoMVC.Controllers
{

    [Authorize]
    public class BaseController : Controller
    {
    
      
        protected SunbeamDBEntities dbobject { get; set; }

        
        public BaseController()
        {
            this.dbobject = new SunbeamDBEntities();
        }
    }
}