﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoMVC.Models;
using System.Web.Security;

namespace DemoMVC.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        SunbeamDBEntities dbobject = new SunbeamDBEntities();

        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(HRLoginInfo loginObject, string ReturnUrl)
        {
            var matchCount = (from hr in dbobject.HRLoginInfoes.ToList()
                         where hr.Username.ToLower() == loginObject.Username.ToLower()
                         && hr.Password == loginObject.Password
                         select hr).ToList().Count();

            if (matchCount == 1)
            {
                FormsAuthentication.SetAuthCookie(loginObject.Username, false);
                if (ReturnUrl!=null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index");
                }
            }
            else
            {
                ViewBag.errorMsg = "username or pwd is incorrect";
                return View();
            }

        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }
    }
}