﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter Report Type Needed");
            Console.WriteLine("1.PDF, 2.WORD, 3.EXCEL");

            int choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    PDF obj = new PDF();
                    obj.Parse();
                    obj.Validate();
                    obj.Save();
                    break;

                case 2:
                    Excel obj1 = new Excel();
                    obj1.Parse();
                    obj1.Validate();
                    obj1.Save();
                    break;

                case 3:
                    Word w = new Word();
                    w.Parse();
                    w.Validate();
                    w.Save();
                    break;

                default:
                    Console.WriteLine("Invalid Choice");
                    break;
            }

            Console.ReadLine();
        }
    }

    public class PDF
    {
        public void Parse()
        {
            Console.WriteLine("PDF Parsing is done");
        }

        public void Validate()
        {
            Console.WriteLine("PDF Validation is done");
        }

        public void Save()
        {
            Console.WriteLine("PDF Saving is done");
        }
    }

    public class Excel
    {
        public void Parse()
        {
            Console.WriteLine("Excel Parsing is done");
        }

        public void Validate()
        {
            Console.WriteLine("Excel Validation is done");
        }

        public void Save()
        {
            Console.WriteLine("Excel Saving is done");
        }
    }

    public class Word
    {
        public void Parse()
        {
            Console.WriteLine("Word Parsing is done");
        }

        public void Validate()
        {
            Console.WriteLine("Word Validation is done");
        }

        public void Save()
        {
            Console.WriteLine("Word Saving is done");
        }
    }
}
