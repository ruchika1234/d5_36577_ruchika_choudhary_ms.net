﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01Demo
{
    class Program
    {
        //Developer 4
        static void Main(string[] args)
        {

            Console.WriteLine("Enter Report Type Needed");
            Console.WriteLine("1.PDF, 2.EXCEL, 3.WORD, 4.Text");

            int choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    PDF obj = new PDF();
                    obj.Generate();
                    break;

                case 2:
                    Excel obj1 = new Excel();
                    obj1.Generate();
                    break;

                case 3:
                    Word w = new Word();
                    w.Generate();
                    break;

                case 4:
                    Text t = new Text();
                    t.Generate();
                    break;

                default:
                    Console.WriteLine("Invalid Choice");
                    break;
            }

            Console.ReadLine();
        }
    }

    /*  public class Report
      {
          public virtual void Parse()
          {
              Console.WriteLine("Report Parsing is done");
          }

          public virtual void Validate()
          {
              Console.WriteLine("Report Validation is done");
          }

          public virtual void Save()
          {
              Console.WriteLine("Report Saving is done");
          }
      } */


    public abstract class Report
    {
        protected abstract void Parse();

        protected abstract void Validate();

        protected abstract void Save();

        public virtual void Generate()
        {
            Parse();
            Validate();
            Save();
        }
    }
    //Developer 1
    public class PDF: Report
    {
        protected override void Parse()
        {
            Console.WriteLine("PDF Parsing is done");
        }

        protected override void Validate()
        {
            Console.WriteLine("PDF Validation is done");
        }

        protected override void Save()
        {
            Console.WriteLine("PDF Saving is done");
        }

    }

    //Developer 2
    public class Excel: Report
    {
        protected override void Parse()
        {
            Console.WriteLine("Excel Parsing is done");
        }

        protected override void Validate()
        {
            Console.WriteLine("Excel Validation is done");
        }

        protected override void Save()
        {
            Console.WriteLine("Excel Saving is done");
        }
    }

    //Developer 3
    public class Word: Report
    {
        protected override void Parse()
        {
            Console.WriteLine("Word Parsing is done");
        }

        protected override void Validate()
        {
            Console.WriteLine("Word Validation is done");
        }

        protected override void Save()
        {
            Console.WriteLine("Word Saving is done");
        }

    }

    public class Text : Report
    {
        protected override void Parse()
        {
            Console.WriteLine("Word Parsing is done");
        }

        protected override void Validate()
        {
            Console.WriteLine("Word Validation is done");
        }

        protected override void Save()
        {
            Console.WriteLine("Word Saving is done");
        }


        protected void ReValidate()
        {
            Console.WriteLine("Word ReValidation is done");
        }

        public override void Generate()
        {
            Parse();
            Validate();
            ReValidate();
            Save();

        }
    }
}
