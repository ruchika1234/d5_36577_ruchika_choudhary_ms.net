﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using second;
using second.third;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            //second.Maths m = new second.Maths()
            Maths m = new Maths();
            Console.WriteLine(m.Add(10, 10));

            test t = new test();
            Console.WriteLine(t.sample());

            DB d = new DB();
            d.insert();

            Console.ReadLine();

        }
    }
}

namespace second
{
    public class Maths
    {
        public int Add(int x, int y)
        {
            return x + y;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("This is MAIN Method in maths class");
        }
    }

    public class test
    {
        public string sample()
        {
            return "this is test";
        }
    }

    namespace third
    {
        public class DB
        {
            public void insert()
            {
                Console.WriteLine("Data inserted successfully");
            }

            static void Main(string[] args)
            {
                Console.WriteLine("This is MAIN Method in DB class");
            }
        }
    }
}
