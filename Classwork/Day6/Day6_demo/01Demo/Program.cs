﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Text;
using System.Threading.Tasks;

namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            string iscontinue = "y";

            ArrayList arr = new ArrayList();


            do
            {
                Console.WriteLine("Enter Your Choice");
                Console.WriteLine("1.Employee 2. Book");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:

                        Console.WriteLine("Enter the No.");
                        Employee emp = new Employee();
                        emp.No = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter the Name");

                        emp.Name = Console.ReadLine();

                     
                        arr.Add(emp);


                          break;

                    case 2:

                        Console.WriteLine("Enter the title");
                        Book bk = new Book();
                        bk.title = Console.ReadLine();
                        Console.WriteLine("Enter the author");

                        bk.Author = Console.ReadLine();

                        arr.Add(bk);

                       

                        break;

                    default:
                        Console.WriteLine("Invalid choice");
                        break;

                }

                Console.WriteLine("would you like to continue Y/N ?");
                iscontinue = Console.ReadLine();

                if(iscontinue == "n")
                {
                    for (int i = 0; i < arr.Count; i++)
                    {
                        if (arr[i] is Employee)
                        {
                            Employee emp = (Employee)arr[i];
                            Console.WriteLine(emp.getDetails());
                        }
                        else if (arr[i] is Book)
                        {
                            Book b = (Book)arr[i];
                            Console.WriteLine(b.getBookDetails());
                        }
                    }
                    Console.ReadLine();
                }

            } while (iscontinue == "y");


        }
    }

    public class Employee
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name;
        }

    }

    public class Book
    {
        private string _title;
        private string _Author;

        public string Author
        {
            get { return _Author; }
            set { _Author = value; }
        }


        public string title
        {
            get { return _title; }
            set { _title = value; }
        }


        public string getBookDetails()
        {
            return this.title + this.Author;
        }

    }
}
