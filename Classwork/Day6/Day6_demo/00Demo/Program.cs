﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "Ruchi";

            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "Rachna";

            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "Pankaj";

            #region Simple type casting

            /*
                        Emp e = new Emp();
                        e.No = 1;
                        e.Name = "Ruchi";

                        int i = 100;
                        Object obj = new Object();

                        //object obj = i;

                        obj = i;
                        obj = "abcd";
                        obj = e;

                        if (obj is int)
                        {
                            int j = Convert.ToInt32(obj);
                            Console.WriteLine(j);
                        }

                        else if (obj is string)
                        {
                            string s = Convert.ToString(obj);
                            Console.WriteLine(s);
                        }

                        else if (obj is Emp)
                        {
                            Emp e1 = (Emp)obj;
                            // Emp e2 = obj as Emp
                            Console.WriteLine(e1.getDetails());
                        }*/


            #endregion

            #region Simple Integer Array - i

            /*  int[] arr = new int[3];
              arr[0] = 100;
              arr[1] = 200;
              arr[2] = 300;
              //   arr[3] = 400; // array out of bound exception;

              for (int i = 0; i < arr.Length; i++)
              {
                  Console.WriteLine(arr[i]);
              }*/

            #endregion

            #region Simple Integer Array - ii
            /*
                        int[] arr = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90 };

                        for (int i = 0; i < arr.Length; i++)
                        {
                            Console.WriteLine(arr[i]);
                        }*/

            #endregion

            #region Emp Array

            /*  Emp[] employees = new Emp[3];
              employees[0] = e1;
              employees[1] = e2;
              employees[2] = e3;

              for (int i = 0; i < employees.Length; i++)
              {
                  Emp e = employees[i];
                  Console.WriteLine(e.getDetails());
              }*/


            #endregion

            #region Object Array of fixed size

            /*object[] obj = new object[5];
            obj[0] = e1;
            obj[1] = 100; // Boxing
            obj[2] = "abcd";
            obj[3] = e2;
            obj[4] = false;

            for (int i = 0; i < obj.Length; i++)
            {
                if(obj[i] is int)
                {
                    int k = Convert.ToInt32(obj[i]);
                    Console.WriteLine(k);
                }
                else if (obj[i] is string)
                {
                    string s = Convert.ToString(obj[i]);
                    Console.WriteLine(s);
                }
                else if (obj[i] is Emp)
                {
                    Emp e = (Emp)obj[i];
                    Console.WriteLine(e.getDetails());
                }
                else if (obj[i] is bool)
                {
                    bool b = Convert.ToBoolean(obj[i]); // unboxing
                    Console.WriteLine(b);
                }
                else
                {
                    Console.WriteLine("Unknown type of data");
                }
            }
*/
            #endregion

            #region ArrayList :  Array of objects .. no limit
            /*
                        ArrayList arr = new ArrayList();
                        arr.Add(e1);
                        arr.Add(100);
                        arr.Add("abcd");
                        arr.Add(e2);
                        arr.Add(false);

                        for (int i = 0; i < arr.Count; i++)
                        {
                            if (arr[i] is int)
                            {
                                int k = Convert.ToInt32(arr[i]);
                                Console.WriteLine(k);
                            }
                            else if (arr[i] is string)
                            {
                                string s = Convert.ToString(arr[i]);
                                Console.WriteLine(s);
                            }
                            else if (arr[i] is Emp)
                            {
                                Emp e = (Emp)arr[i];
                                Console.WriteLine(e.getDetails());
                            }
                            else if (arr[i] is bool)
                            {
                                bool b = Convert.ToBoolean(arr[i]); // unboxing
                                Console.WriteLine(b);
                            }
                            else
                            {
                                Console.WriteLine("Unknown type of data");
                            }
                        }*/

            #endregion

            #region HashTable: Key-value Pair Collection. key value are objects. no limit

            Hashtable arr = new Hashtable();
            arr.Add("a", e1);
            arr.Add("b", 100);
            arr.Add("c", "abcd");
            arr.Add("d", e2);
            arr.Add("e", false);
            arr.Add("g", 10.2);
            arr.Add("f", new DateTime());


            foreach (object key in arr.Keys)
            {
                Console.WriteLine(key);
                object obj = arr[key];
                if (obj is int)
                {
                    int k = Convert.ToInt32(obj);
                    Console.WriteLine(k);
                }
                else if (obj is string)
                {
                    string s = Convert.ToString(obj);
                    Console.WriteLine(s);
                }
                else if (obj is Emp)
                {
                    Emp e = (Emp)obj;
                    Console.WriteLine(e.getDetails());
                }
                else if (obj is bool)
                {
                    bool b = Convert.ToBoolean(obj); // unboxing
                    Console.WriteLine(b);
                }
                else
                {
                    Console.WriteLine("Unknown type of data");
                }

                Console.WriteLine("What would you like to Search? pls enter key");
                string key1 = Console.ReadLine();

                object obj1 = arr[key1];


                for (int i = 0; i < arr.Count; i++)

                    if (obj1 is int)
                    {
                        int k = Convert.ToInt32(obj1);//UnBoxing
                        Console.WriteLine(k);
                    }
                    else if (obj1 is string)
                    {
                        string s = Convert.ToString(obj1);
                        Console.WriteLine(s);
                    }
                    else if (obj1 is Emp)
                    {
                        Emp e = (Emp)obj;

                        Console.WriteLine(e.getDetails());
                    }
                    else if (obj1 is bool)
                    {
                        bool b = Convert.ToBoolean(obj1);//UnBoxing
                        Console.WriteLine(b);
                    }
                    else
                    {
                        Console.WriteLine("unknown type of data!");
                    }
            

            }
        }



            #endregion

           // Console.ReadLine();

        }

    }

    public class Emp
    {
        private int _No;
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name;
        }
    }

}
