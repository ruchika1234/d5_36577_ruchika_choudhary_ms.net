﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace _Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the Assembly Path : EXE / DLL built with .NET");
            string PathOfAssembly = Console.ReadLine();

            // @"G:\MS.Net\Classwork\Day9\Day9_Demos\MathLib\bin\Debug\MathLib.dll"

            //below code is for loading top level TYPE metadata information from MathsLib.dll
            Assembly assembly = Assembly.LoadFrom(PathOfAssembly);

            //Now we ask assembly object rest of the details....
            Type[] allTypes = assembly.GetTypes();

            object dynamicObject = null;

            foreach (Type type in allTypes)
            {
                Console.WriteLine("Type: " + type.Name);

                List<Attribute> allAttributes = new List<Attribute>().ToList();
                bool isSerialize = false;

                foreach (Attribute attribute in allAttributes)
                {
                    if (attribute is SerializableAttribute)
                    {
                        isSerialize = true;
                        break;
                    }
                }

                if (isSerialize)
                {
                    Console.WriteLine(type.Name + " is serializable");
                }
                else
                {
                    Console.WriteLine(type.Name + " is not marked as serializable");
                }

                dynamicObject = assembly.CreateInstance(type.FullName);

                MethodInfo[] allMethods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo method in allMethods)
                {
                    Console.Write(method.ReturnType + "  " + method.Name + "  ( ");

                    ParameterInfo[] allParams = method.GetParameters();

                    object[] arguments = new object[allParams.Length];

                    for (int i = 0; i < allParams.Length; i++)
                    {
                        Console.WriteLine("Enter " + allParams[i].ParameterType.ToString() + "Value for" + allParams[i].Name);

                        arguments[i] = Convert.ChangeType(Console.ReadLine(), allParams[i].ParameterType);
                    }

                    /*foreach (ParameterInfo para in allParams)
                    {
                        Console.Write(para.ParameterType.ToString() + "  " + para.Name);
                    }

                    Console.Write("  ) ");
                    Console.WriteLine();*/

                   // object[] allPara = new object[] { 10, 20 };
                    object result = type.InvokeMember(method.Name,
                                    BindingFlags.Public |
                                    BindingFlags.Instance |
                                    BindingFlags.InvokeMethod,
                                    null, dynamicObject, arguments);

                    Console.WriteLine("Result of" + method.Name + "is execute " + result.ToString());
                }

                

            }

            
            Console.ReadLine();

        }
    }
}
