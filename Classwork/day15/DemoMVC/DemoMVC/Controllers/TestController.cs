﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoMVC.Models;

namespace DemoMVC.Controllers
{
    public class TestController : Controller
    {
        SunbeamDBEntities dbobject = new SunbeamDBEntities();
        
        // GET: Test
        public ActionResult Show()
        {
            try
            {
                var employess = dbobject.Emps.ToList();

                return View(employess);

            }
            catch (Exception ex)
            {

                return View("Error",ex);
            }
        }
        public ActionResult Edit(int id)
        {
            try
            {
                Emp empedited = (from emp in dbobject.Emps.ToList() where emp.Id == id select emp).First();
                return View(empedited);

            }
            catch (Exception ex)
            {

                return View ("Error",ex);
            }
        }
        public ActionResult AfterEdit(FormCollection entireform)
        {
            try
            {
                int idofemp = Convert.ToInt32(entireform["Id"]);
                Emp empedited = (from emp in dbobject.Emps.ToList() where emp.Id == idofemp select emp).First();
                empedited.Name = entireform["Name"].ToString();
                empedited.Address = entireform["Address"].ToString();
                dbobject.SaveChanges();
                return Redirect("/Test/Show");

            }
            catch (Exception ex)
            {

                return View("Error",ex);
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                Emp empDeleted = (from emp in dbobject.Emps.ToList() where emp.Id == id select emp).First();
                dbobject.Emps.Remove(empDeleted);
                dbobject.SaveChanges();
                return Redirect("/Test/Show");
            }
            catch (Exception ex)
            {

                return View("Error",ex);
            }

        }
        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {

                return View("Error",ex); 
            }
        }

        public ActionResult AfterCreate(FormCollection entireform)
        {
            try
            {
                Emp empcreate = new Emp()
                {
                    Id = Convert.ToInt32(entireform["Id"]),
                    Name = entireform["Name"].ToString(),
                    Address = entireform["Address"].ToString()

                };
                dbobject.Emps.Add(empcreate);
                dbobject.SaveChanges();
                return Redirect("/Test/Show");
            }
            catch (Exception ex)
            {

                return View("Error",ex);
            }
        }

    }
}