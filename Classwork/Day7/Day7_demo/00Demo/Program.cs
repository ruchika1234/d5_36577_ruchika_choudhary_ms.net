﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            A obj = new Maths();
            Console.WriteLine(obj.Add(10, 20));

            B obj1 = new Maths();
            Console.WriteLine(obj1.Add(10, 20));

            /*A obj = new Maths();
            Console.WriteLine(obj.Sub(10,20));*/

           /* Maths m = new Maths();
            int result = m.Add(10, 20);
            int result2 = m.Sub(20, 10);

            Console.WriteLine(result);
            Console.WriteLine(result2);*/


            /*SQLServer s = new SQLServer();
            s.Insert();*/

            Console.ReadLine();
        }
    }

    public interface A
    {
        int Add(int x, int y);
        int Sub(int x, int y);
    }

    public interface B
    {
        int Add(int x, int y);
        int Mul(int x, int y);
    }


    public class Maths : A, B
    {
        int A.Add(int x, int y)
        {
            return x + y + 200;
        }

        int B.Add(int x, int y)
        {
            return x + y;
        }

        int B.Mul(int x, int y)
        {
            return x * y;
        }

        int A.Sub(int x, int y)
        {
            return x - y;
        }
    }
    /*
        public class Maths : A, B
        {
            public int Add(int x, int y)
            {
                return x + y;
            }

            public int Mul(int x, int y)
            {
                return x * y;
            }

            public int Sub(int x, int y)
            {
                return x - y;
            }
        }*/

    public interface Database
    {
        void Insert();
        void Update();

        void Delete();
    }

    public class SQLServer
    {
        public void Insert()
        {
            Console.WriteLine("SQL Server data inserted");
        }

        public void Update()
        {
            Console.WriteLine("SQL Server data updated");
        }

        public void Delete()
        {
            Console.WriteLine("SQL Server data Deleted");
        }
    }

    public class Oracle
    {
        public void Insert()
        {
            Console.WriteLine("Oracle data inserted");
        }

        public void Update()
        {
            Console.WriteLine("Oracle data updated");
        }

        public void Delete()
        {
            Console.WriteLine("Oracle data Deleted");
        }
    }
}
