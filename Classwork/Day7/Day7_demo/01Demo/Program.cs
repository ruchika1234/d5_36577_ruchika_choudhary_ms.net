﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01Demo
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Normal class with generic method
            //Maths obj = new Maths();
            //int result = obj.Add(10, 20);
            //Console.WriteLine(result);

            //int p1 = 10;
            //int q1 = 20;
            //Console.WriteLine("Before Swap p1 = " + p1.ToString() + " and q1 = " + q1.ToString());

            //obj.Swap<int>(ref p1, ref q1);

            //Console.WriteLine("After Swap p1 = " + p1.ToString() + " and q1 = " + q1.ToString());
            #endregion

            #region Swap of intergers
            /*
                        Maths m = new Maths();

                        int p = 100;
                        int q = 200;

                        Console.WriteLine("Before Swap p = " + p.ToString() + " and q = " + q.ToString());

                        m.swap(ref p, ref q);

                        Console.WriteLine("After Swap p = " + p.ToString() + " and q = " + q.ToString());
                        Console.ReadLine();*/
            #endregion

            #region Swap of integrs and double
            /*
                        Maths<int> obj = new Maths<int>();

                        int p1 = 100;
                        int q1 = 200;

                        Console.WriteLine("Before Swap p1 = " + p1.ToString() + " and q1 = " + q1.ToString());

                        obj.swap(ref p1, ref q1);

                        Console.WriteLine("After Swap p1 = " + p1.ToString() + " and q1 = " + q1.ToString());


                        Maths<double> obj1 = new Maths<double>();

                        double p2 = 100.10;
                        double q2 = 200.20;

                        Console.WriteLine("Before Swap p2 = " + p2.ToString() + " and q2 = " + q2.ToString());

                        obj1.swap(ref p2, ref q2);

                        Console.WriteLine("After Swap p2 = " + p2.ToString() + " and q2 = " + q2.ToString());

                        Console.ReadLine();*/

            #endregion

            #region Using Normal Derived Class object to call Generic Base Class Method

            /* specialMaths sm = new specialMaths();
             double p = 10.10;
             double q = 20.20;

             Console.WriteLine("Before Swap p = " + p.ToString() + " and q = " + q.ToString());

             sm.swap(ref p, ref q);

             Console.WriteLine("After Swap p = " + p.ToString() + " and q = " + q.ToString());

             Console.ReadLine();*/
            #endregion

            #region  using generic method accepting and returning multiple Generic parameters

            specialMaths<int, double, short, string> obj = new specialMaths<int, double, short, string>();

            short data = obj.doSomething(100, 20.2, 2, "abcd");
            Console.WriteLine(data);
            Console.ReadLine();

            #endregion

        }
    }

    #region  Generic Class ( SpecialMaths) inheriting another generic Class (Maths)

    public class Maths<T>
    {
        public void swap(ref T x, ref T y)
        {
            T z;
            z = x;
            x = y;
            y = z;

        }
    }

    public class specialMaths<P, Q, R, S>: Maths<R>
    {
        public R doSomething(P p, Q q, R r, S s)
        {
            return r;
        }
    }

    #endregion

        #region Normal Class (SpecialMaths)Inheriting from Generic Class (Maths) with T as hardcoded double
/*
    public class Maths<T>
    {
        public void swap(ref T x, ref T y)
        {
            T z;
            z = x;
            x = y;
            y = z;

        }
    }
    
    public class specialMaths: Maths<double>
    {

    }*/

        #endregion

        #region Generic Class (Maths) with Generic(Swap) & Normal Method(Add)

        /*  public class Maths<T>
          {
              public void swap(ref T x, ref T y)
              {
                  T z;
                  z = x;
                  x = y;
                  y = z;

              }

              public int Add(int x, int y)
              {
                  return x + y;
              }
          }*/
        #endregion

        #region Normal Class (maths)with Genric(Swap) & Normal Method(Add)
        /*
            public class Maths
            {
                public void swap<T>(ref T x, ref T y){
                    T z;
                    z = x;
                    x = y;
                    y = z;

                }
            }*/

        #endregion
    }
