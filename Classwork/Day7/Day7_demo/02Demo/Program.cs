﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Sample Employee Objects Used through out the demo
            Emp e1 = new Emp();
            e1.No = 1;
            e1.Name = "Rajiv";

            Emp e2 = new Emp();
            e2.No = 2;
            e2.Name = "Rahul";

            Emp e3 = new Emp();
            e3.No = 3;
            e3.Name = "Mahesh";
            #endregion

            #region List<Emp> generic Collection of EM ... no limit. .only emp objects as<T> is EMP

            /*  List<Emp> arr = new List<Emp>();
              arr.Add(e1);
              arr.Add(e2);
              arr.Add(e3);

              foreach (Emp emp in arr)
              {
                  Console.WriteLine(emp.getDetails());
              }

              Console.ReadLine();*/
            #endregion

            #region Use List like ArrayList, List of generic type

            /*      List<object> arr = new List<object>();
                  List<Utility<int>> utilities = new List<_02Demo.Utility<int>>();
                  Console.ReadLine();*/

            #endregion

            #region Stack<Emp>
            /*
                        Stack<Emp> arr = new Stack<Emp>();
                        arr.Push(e1);
                        arr.Push(e2);
                        arr.Push(e3);
                        arr.Pop();

                        foreach (Emp emp in arr)
                        {
                            Console.WriteLine(emp.getDetails());
                        }*/

            /*   Emp e = arr.Pop();
               Console.WriteLine(e.getDetails());
   */
            //Console.ReadLine();

            #endregion

            #region Queue<Emp>

            /*  Queue<Emp> arr = new Queue<Emp>();
              arr.Enqueue(e1);
              arr.Enqueue(e2);
              arr.Enqueue(e3);

              arr.Dequeue();

              foreach (Emp emp in arr)
              {
                  Console.WriteLine(emp.getDetails());
              }

              Console.ReadLine();*/

            #endregion

            #region Dictionary<int, Emp>

            Dictionary<int, Emp> arr = new Dictionary<int, Emp>();
            arr.Add(e1.No, e1);
            arr.Add(e2.No, e2);
            arr.Add(e3.No, e3);

            foreach (int key in arr.Keys)
            {
                Console.WriteLine("Data available at " + key.ToString());
                Emp e = arr[key];
                Console.WriteLine(e.getDetails());
            }
            Console.ReadLine();

            #endregion
        }
    }


   /* public class Utility<T>
    {

    }*/

    public class Emp
    {
        private int _No;

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string getDetails()
        {
            return this.No.ToString() + this.Name;
        }

    }
}
