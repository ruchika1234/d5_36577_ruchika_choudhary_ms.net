﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace DemoMVC.Helpers
{
    public class SBValidator: ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool IsItValid = false;
            if (value != null)
            {
                if (value.ToString() == "1234")
                {
                    IsItValid = false;
                }
                else
                {
                    IsItValid = true;
                }
            }
            return IsItValid;
        }
    }
}