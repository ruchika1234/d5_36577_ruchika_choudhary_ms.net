//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DemoMVC.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using DemoMVC.Helpers;
    
    public partial class Emp
    {
        [Required(ErrorMessage = "No is must")]
        [Range(1,100,ErrorMessage = "Number must be between 1 to 100")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is must")]
        [SBValidator(ErrorMessage ="1234 is not a valid name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address is must")]

        public string Address { get; set; }
    }
}
