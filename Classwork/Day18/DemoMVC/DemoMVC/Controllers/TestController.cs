﻿using DemoMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoMVC.Controllers
{
    public class TestController : BaseController
    {
        // GET: Test
        public ActionResult Index(int? id)
        {

            ActionResult result = null;
            switch (id)
            {
                case 1:
                    Emp emp = new Emp() { Id = 10, Name = "Rahul", Address = "Ratlam" };
                    result = View(emp);
                    break;
                case 2:
                    result = new JsonResult()
                    {
                        Data = dbobject.Emps.ToList(),
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    break;
                case 3:
                    result = new JavaScriptResult() { Script = "alert('Hello from JS')" };
                    break;
                case 4:
                    return View("Display");
                    break;
                default:
                    result = new ContentResult() { ContentType = "text/plain", Content = "Hello from Server"};
                    break;
            }

            return result;
        }
    }
}