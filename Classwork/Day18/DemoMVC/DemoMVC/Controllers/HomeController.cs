﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DemoMVC.Models;
using System.Configuration;
using DemoMVC.Filter;

namespace DemoMVC.Controllers
{
    public class HomeController : BaseController
    {

       [CustomFilter]
        public ActionResult Index()
        {
            // ViewBag.MyTitle = "Welcome Home";
            //ViewBag.myMessage = "Welcome to Employee Management Application";
            //ViewBag.myMessage2 = "Hello Ruchiiii";

            ViewBag.Username = User.Identity.Name;
            var allEmployee = dbobject.Emps.ToList();
            return View(allEmployee);
        }

        
        public ActionResult Edit(int id)
        {
            try
            {
                ViewBag.message = "Update recored here";
                Emp empedited = (from emp in dbobject.Emps.ToList() where emp.Id == id select emp).First();
                return View(empedited);

            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }
        }

        [HttpPost]
        public ActionResult Edit(Emp empUpdate)
        {
            try
            {
                ViewBag.Username = User.Identity.Name;

                Emp empedited = (from emp in dbobject.Emps.ToList() where emp.Id == empUpdate.Id select emp).First();
                empedited.Name = empUpdate.Name;
                empedited.Address = empUpdate.Address;
                dbobject.SaveChanges();
                return Redirect("/Home/Index");

            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }
        }
        public ActionResult Delete(int id)
        {
            try
            {
                Emp empDeleted = (from emp in dbobject.Emps.ToList() where emp.Id == id select emp).First();
                dbobject.Emps.Remove(empDeleted);
                dbobject.SaveChanges();
                return Redirect("/Home/Index");
            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }

        }
        public ActionResult Create()
        {
            try
            {
                ViewBag.Username = User.Identity.Name;

                return View();
            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }
        }

        [HttpPost]

        public ActionResult Create(Emp empCreate)
        {
            try
            {

                dbobject.Emps.Add(empCreate);
                dbobject.SaveChanges();

                return Redirect("/Home/Index");
            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }
        }

    //    [AllowAnonymous]

        public ActionResult About()
        {
            //ViewBag.MyTitle = "About us";
            return View();
        }

    //    [AllowAnonymous]

        public ActionResult Contact()
        {
            //ViewBag.MyTitle = "COntact us";
            ViewBag.action = "/Home/Contact";
            ViewBag.method = "POST";
            ViewBag.message = "You will error here in case of problem";
            return View();
        }

        [HttpPost]

        public ActionResult Contact(ContactModel contactDetails)
        {
            // string message = contactDetails.ToString();

            try
            {

                string emailUserName = ConfigurationManager.AppSettings["email"];
                string emailPassword = ConfigurationManager.AppSettings["password"];

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailUserName);
                mail.To.Add(contactDetails.Email);
                mail.CC.Add("jatd3445@gmail.com");
                
                mail.Subject = "New Query recieved";
                mail.Body = "<h6>" + contactDetails.ToString() + "</h6>";
                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);


                smtp.Credentials = new NetworkCredential(emailUserName, emailPassword);
                smtp.EnableSsl = true;
                smtp.Send(mail);
                ViewBag.message = "Query submitted successfully";


            }
            catch (Exception ex)
            {

                ViewBag.message = "Some error" + ex.Message;
                return View();

            }
            return null;
        }

    }
}