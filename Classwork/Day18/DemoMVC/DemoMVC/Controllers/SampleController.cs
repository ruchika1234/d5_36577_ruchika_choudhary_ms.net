﻿using DemoMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoMVC.Controllers
{
    public class SampleController : BaseController
    {
        // GET: Demo
        public ActionResult Index()
        {
            // ViewBag.MyTitle = "Welcome Home";
            //ViewBag.myMessage = "Welcome to Employee Management Application";
            //ViewBag.myMessage2 = "Hello Ruchiiii";

            ViewBag.Username = User.Identity.Name;
            var allEmployee = dbobject.Emps.ToList();
            return View(allEmployee);
        }

        [ValidateAntiForgeryToken]

        public ActionResult Edit(int id)
        {
            try
            {
                ViewBag.message = "Update recored here";
                Emp empedited = (from emp in dbobject.Emps.ToList() where emp.Id == id select emp).First();
                return View(empedited);

            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Emp empUpdate)
        {
            try
            {
                ViewBag.Username = User.Identity.Name;

                Emp empedited = (from emp in dbobject.Emps.ToList() where emp.Id == empUpdate.Id select emp).First();
                empedited.Name = empUpdate.Name;
                empedited.Address = empUpdate.Address;
                dbobject.SaveChanges();
                return Redirect("/Sample/Index");

            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }
        }
        public ActionResult Delete(int id)
        {
            try
            {
                Emp empDeleted = (from emp in dbobject.Emps.ToList() where emp.Id == id select emp).First();
                dbobject.Emps.Remove(empDeleted);
                dbobject.SaveChanges();
                return Redirect("/Sample/Index");
            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }

        }
        public ActionResult Create()
        {
            try
            {
                ViewBag.Username = User.Identity.Name;

                return View();
            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Emp empCreate)
        {

            if (ModelState.IsValid)
            {

                dbobject.Emps.Add(empCreate);
                dbobject.SaveChanges();

                return Redirect("/Sample/Index");
            }
            else
            {
                return View(empCreate);
            }

           /* try
            {

                dbobject.Emps.Add(empCreate);
                dbobject.SaveChanges();

                return Redirect("/Sample/Index");
            }
            catch (Exception ex)
            {

                return View("Error", ex);
            }*/
        }

    }
}