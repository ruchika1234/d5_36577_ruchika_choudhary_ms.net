﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace _00Demo
{
    delegate string MyDelegate(string name);
    class Program
    {
        static void Main(string[] args)
        {
            #region Partial class

            /* Maths math = new Maths();

             Console.WriteLine(math.Add(10, 20));
             Console.WriteLine(math.Sub(20, 2));
             Console.WriteLine(math.Mul(10, 20));

 */
            #endregion

            #region NullAble type

            //int? salary = 0;
            /*int? salary = null;

            if (salary.HasValue)
            {
                Console.WriteLine("Salary has a Value");
            }
            else
            {
                Console.WriteLine("Salary is NULL");
            }*/

            #endregion

            #region Anonymous Method

            //Call Normal Method
            //sayHello("Hello World");

            // Call method using pointer
            /* MyDelegate pointer = new MyDelegate(sayHello);
             string msg = pointer("Hello Ruchi");
             Console.WriteLine(msg);*/

            // call method which is anonymous
            MyDelegate pointer = delegate (string name)
            {
                return "Hello " + name;
            };
            string msg = pointer("Ruchi");
            Console.WriteLine(msg);


            /* string sayHello(string message)
             {
                 return message;
                // Console.WriteLine(message);
             }*/

            #endregion

            #region Lambda Expression Syntax
            /*
                        MyDelegate pointer1 = (nm) => { return "Hello " + nm; };
                        string msg1 = pointer1("Ruchika");
                        Console.WriteLine(msg1);*/

            #endregion

            #region iterator



            #endregion

            #region Implicit type Var
            /*
                        int choice = Convert.ToInt32(Console.ReadLine());
                        var v = GetSomethong(choice);
                        Console.WriteLine(v);

                        var v1 = 100;
                        // v = "abcd"; //generate error

                        var v2 = new Emp();*/

            #endregion

            #region Auto Property

            /*  Emp emp = new Emp();
              emp.No = 1;
              emp.Name = "Ruchi";
              emp.Address = "Ratlam";

              emp.age = 22;

              Console.WriteLine(emp.No + emp.Name + emp.Address + emp.age);
  */
            #endregion

            #region MyRegion

            #endregion

            Console.ReadLine();
        }

        public static object GetSomethong(int i)
        {
            if(i == 1)
            {
                return 100;
            }
            else
            {
                return new object();
            }
        }

        public class Emp
        {
            private int _age;

            public int age
            {
                get { return _age; }
                set { _age = value; }
            }

            public int No { get; set; }
            public string Name { get; set; }
            public string Address { get; set; }

        }
    }
}
