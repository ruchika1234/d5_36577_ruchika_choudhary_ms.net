﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoMVC.Models
{
    public class ContactModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Query { get; set; }

        public override string ToString()
        {
          return string.Format("Hello {0}, we have recieved your query: Name: {0}, Email{1}, Phone{2}, Query{3}",
                this.Name , this.Email, this.Phone, this.Query);
        }
    }
}